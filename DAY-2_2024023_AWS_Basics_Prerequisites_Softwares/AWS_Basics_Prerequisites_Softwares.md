#### Session Video:
```
https://drive.google.com/file/d/1dy_hvtt5QjkyTwWi6Zw7r6fBhaIaS-9t/view?usp=sharing
```

#### Agenda :

```
1. Creating Account with GitLab(VCS/SCM) GUI Vendor

2. Creating Account with AWS 

3. Course Prerequisites:
    1. Laptop / Desktop :
        1.1 Operating System :
            1.1.1 Windows or MacOS 

    2. Download, Install & Configure required softwares:
        2.1 Browsers i.e. Chrome 
        2.2 Text Editors i.e. Visual Studio Code
        2.3 VCS/SCM CLI Tool : Git 


4. AWS Basics :
    - Regions
    - Availability Zones
    - AWS Management Console & it's features 

```

