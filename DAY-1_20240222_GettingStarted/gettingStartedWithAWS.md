#### Session Video:
```
https://drive.google.com/file/d/1dp0aW2vK4-J9osZLXGwgK9e_mEw-v1Np/view?usp=sharing
```

#### Agenda :

```
1. Creating Account with GitLab(VCS/SCM) GUI Vendor

2. Creating Account with AWS 

3. Course Prerequisites:
    1. Laptop / Desktop :
        1.1 Operating System :
            1.1.1 Windows or MacOS 

    2. Download, Install & Configure required softwares:
        2.1 Browsers i.e. Chrome 
        2.2 Text Editors i.e. Visual Studio Code
        2.3 VCS/SCM CLI Tool : Git 
```

